package com.example.miniproject1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private SeekBar seekBar;
    private TextView textView;
    private double curRate = 0;
    private RadioButton radioButton;
    private RadioButton radioButton1;
    private RadioButton radioButton2;
    private Button calculate;
    private EditText editText;
    private TextView resTextView;
    private int month;
    private CheckBox checkBox;
    private RadioGroup radioGroup;
    private Button uninstall;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        seekBar = findViewById(R.id.seekBar);
        uninstall = findViewById(R.id.button2);
        textView = findViewById(R.id.interestRate);
        radioButton = findViewById(R.id.radioButton);
        calculate = findViewById(R.id.button);
        radioButton1 = findViewById(R.id.radioButton1);
        radioButton2 = findViewById(R.id.radioButton2);
        radioGroup = findViewById(R.id.radioGroup);
        editText = findViewById(R.id.principle);
        checkBox = findViewById(R.id.checkBox);
        resTextView = findViewById(R.id.res);
        textView.setText(String.valueOf(0.0) + "%");
        seekBar.setMax(200);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                View radioButton4 = radioGroup.findViewById(checkedId);
                int index = radioGroup.indexOfChild(radioButton4);
                switch (index){
                    case 0:
                        radioButton1.setChecked(false);
                        radioButton2.setChecked(false);
                        break;
                    case 1:
                        radioButton.setChecked(false);
                        radioButton2.setChecked(false);
                        break;
                    case 2:
                        radioButton.setChecked(false);
                        radioButton1.setChecked(false);
                }
            }
        });

        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if(editText.getText().toString().isEmpty()){
                     Log.e(null,"heihei");
                     Toast.makeText(getBaseContext(),"The input is not the valid",Toast.LENGTH_LONG).show();
                     return;
                 }
                 String s = editText.getText().toString();
                 try{

                    double p = Double.parseDouble(s);
                    int index = s.indexOf(".");
                    if(index >= 0){
                        s = s.substring( index + 1);
                        if(s.length() >= 3){
                            Toast.makeText(MainActivity.this,"The input is not the valid",Toast.LENGTH_LONG).show();
                            return;
                        }
                    }
                    if(radioButton.isChecked()){
                        month = 15 * 12;
                    }else if(radioButton1.isChecked()){
                        month = 20 * 12;
                    }else{
                        month = 30 * 12;
                    }
                    double tax = checkBox.isChecked() ? 0.1 : 0;
                    double res = getRes(p,tax);
                    resTextView.setText( "Monthly payment " + String.format("%.2f",res) + "$");


                 }catch (Exception e){
                     Toast.makeText(MainActivity.this,"The input is not the valid",Toast.LENGTH_LONG).show();
                     return;
                 }
            }
        });
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                 curRate =  ((double)progress / 10);
                 textView.setText(String.valueOf((double)progress / 10) + "%" );
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        uninstall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }
        });
    }

    public double getRes(double p,double tax){
         if( curRate == 0){
              return p / month + tax;
         }else{
             Log.e(null,String.valueOf(month));
             Log.e(null,String.valueOf(p));
             Log.e(null,String.valueOf(p*(curRate/100/12)));
             return ((p*(curRate/100/12)) / (1 - (Math.pow(1 + (curRate/100/12),-month)))) + (tax / 100 *p);
         }
    }
}