package com.example.miniproject1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class UnitTest {

    @Test
    public void addition_isCorrect() {
        double p = 10000.00;
        double j =5.5 / 100 / 12;
        int month = 12*15;
        double t = 0;
        assertEquals( String.format("%.2f", ((p*(j)) / (1 - (Math.pow(1 + (j),-month)))) + (t / 100 *p) ), "81.71");
    }
    @Test
    public void addition_isCorrect2() {
        double p = 20000.00;

        int month = 20*12;
        double t = 0;
        assertEquals( String.format("%.2f",p / month + t), "83.33");
    }

    @Test
    public void addition_isCorrect3() {
        double p = 10000.00;
        double j =5.5 / 100 / 12;
        int month = 12*15;
        double t = 0.1;
        assertEquals( String.format("%.2f", ((p*(j)) / (1 - (Math.pow(1 + (j),-month)))) + (t / 100 *p) ), "91.71");
    }
}